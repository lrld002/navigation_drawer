package com.android.sgifer.navigationdrawer.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.view.MenuItem
import com.android.sgifer.navigationdrawer.R
import com.android.sgifer.navigationdrawer.fragments.ProfileFragment

class MainActivity : AppCompatActivity() {

    private lateinit var toolbar: android.support.v7.widget.Toolbar
    private var drawerLayout: DrawerLayout? = null
    private var navigationView: NavigationView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViews()
        setListeners()

        setSupportActionBar(toolbar)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_menu_navigation)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        cambiarFragmento(ProfileFragment(), navigationView!!.menu.getItem(0))

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            android.R.id.home -> {
                drawerLayout!!.openDrawer(GravityCompat.START)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun findViews(){
        toolbar = findViewById(R.id.toolbar)
        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.navigationView)
    }

    private fun setListeners(){
        navigationView!!.setNavigationItemSelectedListener { item ->

            var gestorDeFragmentos = false
            var fragment: Fragment? = null

            when(item.itemId){
                R.id.menu_perfil -> {
                    fragment = ProfileFragment()
                    gestorDeFragmentos = true
                }
            }
            if (gestorDeFragmentos){
                cambiarFragmento(fragment!!,item)
                drawerLayout!!.closeDrawers()
            }
            true
        }
    }

    private fun cambiarFragmento(fragment: Fragment?, item: MenuItem) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit()
        item.isChecked = true
        supportActionBar!!.title = item.title
    }

}
